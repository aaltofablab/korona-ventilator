# Korona Ventilator

Repository with a website for an open COVID-19 ventilator for Finland. Access website via https://aaltofablab.gitlab.com/korona-ventilator/

## Structure

The structure of the repository is as follows.

```
├── README.md
├── design
├── website
└── .gitlab-ci.yml
```

`README.md` is this file and works as a manual for the repository.  
`design` is where all the design files go.  
`website` is where the website source files live.  
`.gitlab-ci.yml` describes how the website should be compiled and published. 

## Contribution

1. Create a gitlab.com account if you do not have one already.
2. Add an issue in the issue tracker that you want to be added to the project.

The preferred way to collaborate here would be using pull requests. For that to work we need the following.

1. Set up a trust board with people who can evaluate potential improvements.
2. Use issue tracker to discuss features, develop them and submit as pull requests.

## How Website is Published

We use [Hugo](https://gohugo.io/) for building the site. It is based on [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) content files. A docker container is cloning this repository and using `hugo` to compile the `website` directory into a HTML website and then uploading it to the web.

