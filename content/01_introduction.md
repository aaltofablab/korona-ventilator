---
title: Introduction
menu: main
lead: "We are a self-organized volunteer group striving to build 1000 emergency care **Plan C** ventilators to be used by Covid-19 pandemic patients in the event regular medical equipment is unavailable. The group formed at the beginning of March, when the risk of equipment shortages in Finland during the first weeks of June became a real possibility. The whole group deeply wishes that these devices would never have to be used. "
---

{{< image src="/images/vcf.jpg" alt="VCF Publicity Image" >}}

The design team quickly grew in size and now includes anesthesiologists, equipment designers, modelling and simulation experts, nurses and paramedics. The design is converging to a device that would be capable of supporting patients that are not yet in the worst stages of the disease, freeing up more advanced medical equipment to patients that need higher levels of care.

The design process has followed guidelines of of safety critical systems engineering, but given the current timeline getting medical approval for the model is not feasible. To give an indication of the differences of time scales, normal delivery time from order for medical equipment is around 100 weeks from order, our target delivery time is 6 weeks. In this circumstance it is foreseen that responsible doctors would use the device as a last measure, in case nothing better is available for the patients. This approach has been confirmed feasible from the doctors embedded in the team.

The commercial/legal structure of the group is still under consideration of volunteer lawyers. We emphasize that under no circumstances the designers and manufacturers would be responsible for any harmful effects that might take place due to use of the equipment. 

The current design goal is to produce the design, manufacturing files and source code and release them under open source licenses. A prototype would also be built to demonstrate the feasibility and effectiveness of the design. The final series production would then be done under the medical device manufacturers that are also participating in the group. This final step would require the go-ahead from the government and this is highly dependent on the dynamic situation of public health and medical resources under pandemic conditions. 

There are further complications to manufacturing. Critical components are already being sold out globally due to increased interest in building medical ventilators. This means that if the consortium wants to be able to build the devices, a safety stock of critical components needs to be established. This is to ensure the manufacturing is actually possible if the need arises. 

The funding requirements thus come in two variations, first a prototype needs to be built and second that the safety stock needs to be established. The first requirement is in the order of ten to twenty thousand Euro, and the second is likely double as much. The actual target cost for a device is between two and three thousand Euro. 

Final caveat for the project is that current estimations expect medical resources to run out in order of staff, consumables and only last equipment. So there is a growing focus on ensuring the availability of the first two on the expense of the third. We, however, feel that this important detail should not be forgotten. Even in the best case scenario that the device will never be used, it is a proven prototype for an emergency care ventilator which can then be further developed into a commercial product that could be used for example in disaster areas or as emergency relief for areas which are more harder hit by the pandemic.

On behalf of VCF   
Paavo Heiskanen  
[paavo.heiskanen@iki.fi](mailto:paavo.heiskanen@iki.fi)
