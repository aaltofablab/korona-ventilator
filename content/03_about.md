---
title: About
menu: main
lead: "Here you can find a few links related to the VCF project."
---

- [VCF Basic Information](/files/20200401_VCF_Basic_info.pdf)
- [VCF Charter](/files/Charter.pdf)
- [Press Release in Finnish 31 March](/files/Press_FI_2020-03-31.pdf)
- [Publicity Image](/images/vcf-publicity.jpg)
